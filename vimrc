" be iMproved
set nocompatible 

filetype off

" Highlight syntax
syntax on

" 256 colors
set t_Co=256

" Indentation
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent

" Show line numbers
set number

" No Swap file
set noswapfile

" Vundle 
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Vundle repository in github
Bundle 'gmarik/vundle'

" Zenburn colorscheme
Bundle 'Zenburn'
colorscheme zenburn

" Autocompletion for Python
Bundle 'davidhalter/jedi-vim'

Bundle 'scrooloose/nerdtree'
nmap <silent> <F3> :NERDTreeToggle<CR>
let NERDTreeIgnore = ['\.pyc$']

Bundle 'nvie/vim-flake8'

filetype plugin indent on " required
